# deltaRena Judge API Client

Refer to the [documentation](https://deltarena.gitlab.com/docs).

Note that, because of the usage of [async generators](https://github.com/tc39/proposal-async-iteration),
TypeScript >=2.3 and Node.js >= 10 must be used.
However, most modern browsers (Firefox Quantum+, Chrome, Edge, ...) support this feature.
