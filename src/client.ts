import WebSocket from "ws";
import FailRequest from "./fail";
import SubmissionInfo from "./submission";
import UpdateRequest from "./update";
import ws from "./ws";

/**
 * The main API client. Construct a new instance of Client,
 * or use the default client to begin.
 *
 * In the event of any failure, remove the instance (by calling `terminate`)
 * and create a new one.
 */
export default class Client {
  private connection: Promise<WebSocket>;

  /**
   * @param host The deltaRena server host.
   * @param api_key The API key for the client.
   */
  constructor(host: string, opts: { APIKey: string; tags: string[] }) {
    this.connection = ws(host, opts);

    /// Setup a connection refresher
    const refresher = (conn: WebSocket) => {
      const handler = () => {
        if (conn.readyState !== WebSocket.OPEN) {
          conn.removeAllListeners();
          conn.terminate();
          this.connection = ws(host, opts);
          this.connection.then(refresher);
        }
      };
      conn.on("error", handler);
      conn.on("close", handler);
    };
    this.connection.then(refresher);
  }

  // Public functions

  /**
   * Awaits the initialization of the WebSocket.
   */
  public async init(tags: string[]) {
    const conn = await this.connection;
    return new Promise((resolve, reject) => {
      conn.send({ event: "init", data: { tags } }, err =>
        err ? reject(err) : resolve
      );
    });
  }

  /**
   * Request is an async generator of SubmissionInfos.
   *
   * Note that the generator might throw in the middle of generating,
   * and it is the responsibility of the user to catch and re-call
   * the generator.
   */
  public async *Request() {
    const receive = async () => {
      const conn = await this.connection;
      return new Promise<SubmissionInfo>(resolve => {
        conn.once("message", data => {
          const msg = JSON.parse(data.toString());
          if (msg.event === "submission") {
            resolve(msg.data);
          }
        });
      });
    };
    while (true) {
      yield await receive();
    }
  }

  /**
   * Updates the score.
   */
  public Update(r: UpdateRequest): Promise<void> {
    return new Promise(async (resolve, reject) => {
      const conn = await this.connection;
      conn.send({ event: "update", data: r }, err =>
        err ? reject(err) : resolve()
      );
    });
  }

  /**
   * Send a failure.
   */
  public async Fail(r: FailRequest): Promise<void> {
    return new Promise(async (resolve, reject) => {
      const conn = await this.connection;
      conn.send({ event: "fail", data: r }, err =>
        err ? reject(err) : resolve()
      );
    });
  }
}
