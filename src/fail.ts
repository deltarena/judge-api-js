/**
 * The fail request interface.
 * See docs.
 */
export default interface FailRequest {
  submissionID: string; // This is unique_id
  reason: string;
  abortProblem: boolean; // If the problem should be taken down for fixing.
}
