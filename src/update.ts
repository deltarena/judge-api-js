/**
 * The score update interface.
 * See docs.
 */
export default interface UpdateRequest {
  submissionID: string; // This is unique_id
  score: number; // A float between 0 and 1, which will be later multiplied into the base score.
  ignoreScoring: boolean; // Whether this submission counts (for penalty-checks and limit counts).
  judgeMessage: {
    title?: string; // Title will be HTML-escaped. Omit to use defaults (Accepted / Partial Score / Wrong Answer)
    type: "markdown" | "text"; // Whether to render it as pure text or Markdown
    content: string; // Note that content will be HTML-escaped.
  };
}
