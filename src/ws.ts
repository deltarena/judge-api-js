import WebSocket from "ws";

export function getWS(
  host: string,
  opts: { APIKey: string; tags: string[] }
): Promise<WebSocket> {
  const p = new Promise<WebSocket>(function(resolve, reject) {
    const ws = new WebSocket(host, {
      headers: {
        Authorization: `Bearer ${opts.APIKey}`
      }
    });
    let resolved = false;
    // Set a timeout
    setTimeout(() => {
      if (!resolved) {
        reject(new Error("Timed out"));
      }
    }, 5000);
    // Set an error catcher
    const errorCatcher = (err: any) => reject(err);
    ws.once("error", errorCatcher);
    ws.once("open", () => {
      ws.removeEventListener("error", errorCatcher);
      resolved = true;
      resolve(ws);
    });
  });
  return p;
}

/**
 *  Attempt to receive a WebSocket connection.
 */
export default async function attemptWS(
  host: string,
  opts: { APIKey: string; tags: string[] }
): Promise<WebSocket> {
  let timeout = 1000;
  while (true) {
    try {
      const client = await getWS(host, opts);

      /// Set up heartbeat
      let pingTimeout: NodeJS.Timeout | null = null;
      function heartbeat() {
        if (pingTimeout) {
          clearTimeout(pingTimeout);
        }

        // Use `WebSocket#terminate()` and not `WebSocket#close()`. Delay should be
        // equal to the interval at which your server sends out pings plus a
        // conservative assumption of the latency.
        pingTimeout = setTimeout(() => {
          client.terminate();
        }, 30000 + 1000);
      }

      client.on("open", heartbeat);
      client.on("ping", heartbeat);

      client.on("close", function clear() {
        if (pingTimeout) {
          clearTimeout(pingTimeout);
        }
      });
      return client;
    } catch (e) {
      console.log(
        `Cannot connect to WS: ${e}. Retrying in ${(timeout / 1000).toFixed(
          1
        )} seconds...`
      );
      await delay(timeout);
      timeout = Math.min(30 * 1000, timeout * 1.5);
    }
  }
}

/**
 * A simple async delay
 */
function delay(ms: number): Promise<void> {
  return new Promise(resolve => setTimeout(resolve, ms));
}
