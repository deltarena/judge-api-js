/**
 * The error-returning interface,
 * as described in the documentation.
 */
export default interface ErrorReturns {
  code: string;
  error: string;
}

/**
 * Returns whether the response is an ErrorReturns struct.
 * @param t A response struct.
 */
export function isError(t: any): t is ErrorReturns {
  return (
    "code" in t &&
    typeof t.code === "string" &&
    "error" in t &&
    typeof t.error === "string"
  );
}
