/**
 * The returned SubmissionInfo interface.
 * See documentation.
 */
export default interface SubmissionInfo {
  uniqueID: string;
  solution: string;
  problem: {
    id: number;
    idealSolution: string;
    meta: any;
  };
  contest: {
    ID: number;
    meta: any;
  };
  contestant: {
    uniqueID: string;
  };
}

/**
 * No submissions in queue.
 */
export interface NoSubmissions {}

/**
 * The response of a successful /judge/request call.
 */
export type SubmissionInfoResponse = SubmissionInfo | NoSubmissions;

export function isSubmissionInfo(
  s: SubmissionInfoResponse
): s is SubmissionInfo {
  return "uniqueID" in s;
}
